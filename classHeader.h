//Header file containing classes and function prototypes

#include <string>

class Attributes
{
public:

	explicit Attributes();

	int getIntelligence() const;
	void setIntelligence(int intelligence);

	int getAgility() const;
	void setAgility(int agility);

	int getStrength() const;
	void setStrength(int strength);

	int getStamina() const;
	void setStamina(int stamina);

	int getMaxHealth() const;
	void setMaxHealth(int maxHealth);

	int getCurrentHealth() const;
	void setCurrentHealth(int currentHealth);

	int getMaxMana() const;
	void setMaxMana(int maxMana);

	int getCurrentMana() const;
	void setCurrentMana(int currentMana);

private:

	int mIntelligence;
	int mAgility;
	int mStrength;
	int mStamina;
	int mMaxHealth;
	int mCurrentHealth;
	int mMaxMana;
	int mCurrentMana;

}

class Equipment
{

}